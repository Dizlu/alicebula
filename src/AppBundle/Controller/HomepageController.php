<?php

namespace AppBundle\Controller;

use Doctrine\DBAL\Types\ArrayType;
use Symfony\Bridge\Twig\Extension\RoutingExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


use AppBundle\Entity\Auction;
use AppBundle\Entity\User;

class HomepageController extends Controller
{
    /**
     * @Route("/login")
     */


    public function loginAction()
    {
        $request = Request::createFromGlobals();

        //$user = $this->createUserAction();
        //$form = $this->newUser();

        return $this->render('AppBundle:Login:login.html.twig', [
            //'user' => $user,
            //'form' => $form->createView()
        ]);

    }

/**
 * @Route("/auction/{id}")
 */
    public function auctionAction($id)
    {

        $auction = $this->showAuctionAction($id);

        return $this->render('AppBundle:Auction:auction.html.twig', [
            'name' => $id,
            'auction' => $auction
        ]);

    }

    /**
     * @Route("/register")
     */

    public function registerAction()
    {

        return $this->render('AppBundle:Register:register.html.twig', [
        ]);

    }

    /**
     * @Route("/panel")
     */
    public function panelAction()
    {
        $request = Request::createFromGlobals();
        $form = $this->newAuction();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $auction = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($auction);
            $em->flush();

            return $this->forward('AppBundle:Homepage:homepage');
        }

        return $this->render('AppBundle:Panel:panel.html.twig', [
            'form' => $form->createView(),
        ]);

    }


    /**
     * @Route("/panel/auctions/{id}")
     */
    public function panelAuctionsAction($id = null)
    {

        if ($id != null) {
            $this->deleteAuctionAction($id);
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $auctions = $this->showAuctionUserAction($user);

        return $this->render('AppBundle:Panel:auctions.html.twig', [
            'auctions' => $auctions,
            'user' => $user
        ]);

    }

    /**
     * @Route("/category/{category}/", name="static")
     */
    public function homepageAction($name = null, $category = 'all')
    {
        $auctions = $this->showAuctionCategoryAction($category);


        return $this->render('AppBundle:Homepage:homepage.html.twig', [
            'name' => $name,
            'auctions' => $auctions,
            'category' => $category
        ]);
    }

    /**
     * @Route("/", name="homepage")
     */
    public function homepageStaticAction($name = null, $category = 'all')
    {
        //$this->createAuctionAction();
        $auctions = $this->showAuctionCategoryAction($category);


        return $this->render('AppBundle:Homepage:homepage.html.twig', [
            'name' => $name,
            'auctions' => $auctions,
            'category' => $category
        ]);
    }

    public function createAuctionAction()
    {
        $auction = new Auction();
        $auction->setUsername('Xiaomi Mi 5');
        $auction->setPrice('300');
        $auction->setCategory('all');
        $auction->setDescription('\'Buy this phone! Man this phone will be amaizing\'');
        $auction->setOwner(1);
        $auction->setSeller('ChinaGuy');
        $auction->setFeatures(['Camera: 21MPX', '4G', 'Snapdragon 720', '3GB RAM', '128GB ROM']);
        $auction->setPhoto('images/xiaomi5s.jpeg');

        $em = $this->getDoctrine()->getManager();

        $em->persist($auction);

        $em->flush();

        return new Response('Saved new product with id '.$auction->getId());
    }

    public function createUserAction()
    {
        $auctions = $this->showAuctionNameAction('Xiaomi Mi 5');


        $user = new User();
        $user->setUsername('Xiang Yog');
        $user->setCountry('China');
        $user->setDescription('Some describtion about him');
        $user->setOnions('143');
        $user->setAuctions($auctions);
        $user->setAuctionsNr([12, 324, 23, 2]);
        $user->setPhoto('images/xiaomi5s.jpeg');
        $user->setLogin('Fred');
        $user->setPassword('1234');
        $user->setDateCreated(new \DateTime("now"));

        $em = $this->getDoctrine()->getManager();

        $em->persist($auctions[0]);
        $em->persist($user);

        $em->flush();

        return new Response('Saved new product with id '.$user->getId());
    }

    public function newAuction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $auction = new Auction();
        $auction->setUsername('Hello');
        $auction->setPrice(0);
        $auction->setCategory('');
        $auction->setDescription('');
        $auction->setOwner($user);
        $auction->setFeatures(['ss']);
        $auction->setPhoto('images/xiaomi5s.jpeg');
        $auction->setDateCreated(new \DateTime("now"));


        $form = $this->createFormBuilder($auction)
            ->add('name', TextType::class)
            ->add('price', MoneyType::class)
            ->add('category', ChoiceType::class,
                array('choices' => array(
                    'All' => 'all',
                    'Deals' => 'deals',
                    'Promoted' => 'promoted')))
            ->add('description', TextareaType::class)
            ->add('features', CollectionType::class, array(
                'entry_type' => TextType::class,
                'allow_add' => true
            ))
            ->add('photo', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();

        return $form;
    }

    public function newUser()
    {
        $auctions = $this->showAuctionNameAction('Xiaomi Mi 5');

        $user = new User();
        $user->setUsername('Xiang Yog');
        $user->setCountry('China');
        $user->setDescription('Some describtion about him');
        $user->setOnions('143');
        $user->setAuctions($auctions);
        $user->setAuctionsNr([12, 324, 23, 2]);
        $user->setPhoto('images/xiaomi5s.jpeg');
        $user->setLogin('MyLogin');
        $user->setPassword('Password');
        $user->setDateCreated(new \DateTime("now"));

        $form = $this->createFormBuilder($user)
            ->add('name', TextType::class)
            ->add('country', TextType::class)
            ->add('description', TextareaType::class)
            ->add('onions', IntegerType::class)
            ->add('login', TextType::class)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();

        return $form;
    }

    public function new_fos_user() {
        $userManager = $container->get('fos_user.user_manager');
    }

    public function showAuctionAction($auctionId)
    {

        $repository = $this->getDoctrine()->getRepository('AppBundle:Auction');

        $auction = $repository->find($auctionId);
        return $auction;
    }

    public function showAuctionCategoryAction($category)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Auction');

        $auction = $repository->findByCategory($category);
        return $auction;
    }

    public function showAuctionNameAction($name)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Auction');

        $auction = $repository->findByName($name);
        return $auction;
    }

    public function showAuctionUserAction($name)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Auction');

        $auction = $repository->findByOwner((string)$name);
        return $auction;
    }

    public function deleteAuctionAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Auction');
        $auction = $repository->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($auction);
        $em->flush();
    }
}
