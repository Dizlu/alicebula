var NoteSection = React.createClass({
    getInitialState: function() {
        return {
            notes: []
        }
    },

    componentDidMount: function() {
        this.loadNotesFromServer();
        setInterval(this.loadNotesFromServer, 2000);
    },

    loadNotesFromServer: function() {
        fetch(this.props.url)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    notes: res.notes
                })
            })
    },

    render: function() {
        return (
            <div>
                <NoteList notes={this.state.notes} />
            </div>
        );
    }
});

var NoteList = React.createClass({
    render: function() {
        var noteNodes = this.props.notes.map(function(note) {
            return (

                <NoteBox username={note.username} avatarUri={note.avatarUri} date={note.date} key={note.id}>{note.note}</NoteBox>
            );
        });

        return (
            <section id="cd-timeline">
                {noteNodes}
            </section>
        );
    }
});

var NoteBox = React.createClass({
    render: function() {
        return (
            <div className="cd-timeline-block">
                <div className="cd-timeline-content">
                    <h2><a href="#">{this.props.username}</a></h2>
                    <p>{this.props.children}</p>
                    <span styleName="text-align: right">{this.props.date}</span>
                </div>
            </div>

        );
    }
});

window.NoteSection = NoteSection;
